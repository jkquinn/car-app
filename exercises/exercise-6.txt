Exercise 6

1. Create an array of car objects. Ensure each car object has all of the properties it needs including the properties displayed in the table and the properties displayed in the details.

2. Pass the array of cars objects as props into the car table component and display the array of cars using the usual method.

The car table component should be displayed for any URL starting with '/cars'

3. When the view button is clicked for a particular car navigate to the following url:

/cars/<whatever the id is>

4. For the route "/cars/<whatever the id is>" display the car with the specified id in the car details component.

5. When a car id is specified in the URL, the table row for that car should stylistically changed to reflect the selected car.

6. Ensure is all works.

