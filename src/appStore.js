import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import { composeWithDevTools } from 'redux-devtools-extension';

import { cars } from './reducers/cars';
import { editCarId } from './reducers/editCarId';
import { requestDeleteCarId } from './reducers/requestDeleteCarId';

export const history = createBrowserHistory();

export const appStore = createStore(connectRouter(history)(combineReducers({
  cars, editCarId, requestDeleteCarId,
})), composeWithDevTools(applyMiddleware(routerMiddleware(history), thunk)));

