import React, { Component } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import dance from '../Images/tenor.985f0059.gif';

class CollapseTest extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = { collapse: false };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    render() {
        return (
            <div>
                <Button color="primary" onClick={this.toggle} style={{ marginBottom: '1rem' }}>Toggle</Button>
                <Collapse isOpen={this.state.collapse}>
                    <Card>
                        <CardBody>
                            <img src={dance} alt={"dance"}/>
                        </CardBody>
                    </Card>
                </Collapse>
            </div>
        );
    }
}

export default CollapseTest;