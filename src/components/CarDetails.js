import React from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle
} from 'reactstrap';

import './CarDetails.css';

export const CarDetails = ({ car }) => {

  return <section className="CarDetails">
    <Card>
      <CardBody>
        <CardTitle>Car Details</CardTitle>
        <CardText><ul>
          <li>Make: {car.make}</li>
          <li>Model: {car.model}</li>
          <li>Year: {car.year}</li>
          <li>Color: {car.color}</li>
          <li>Price: {car.price}</li>
          <li>Transmission: {car.transmission}</li>
          <li>Cruise Control: {car.cruiseControl}</li>
          <li>Heated Seats: {car.heatedSeats}</li>
        </ul></CardText>
      </CardBody>
    </Card>
  </section>;

};