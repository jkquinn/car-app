import React from 'react';
import { Jumbotron} from 'reactstrap';

const Jumbo = ({msg}) => {
  return (
    <div>
      <Jumbotron>
        <h1 className="display-3">{msg}</h1>
        <p className="lead">Kick Babies</p>
        <hr className="my-2" />
        </Jumbotron>
    </div>
  );
};

export default Jumbo;